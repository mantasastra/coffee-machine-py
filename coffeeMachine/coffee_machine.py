class CoffeeMachine:
    def __init__(self):
        self.current_state = None
        self.water_amount = 400
        self.milk_amount = 540
        self.beans_amount = 120
        self.cups_amount = 9
        self.money_amount = 550
        self.buy_options = ['1', '2', '3']

    def handle_input(self, user_input):
        if user_input == 'exit':
            self.current_state = 'exiting'
        elif user_input == 'remaining':
            self.current_state = 'checking remaining stock'
            self.show_amount()
        elif user_input == 'buy':
            self.current_state = 'buying a coffee'
        elif user_input in self.buy_options:
            self.current_state = None
            self.buy(user_input)
        elif user_input == 'back':
            self.current_state = 'going back to the main menu'
        elif user_input == 'fill':
            self.current_state = 'filling up the stock'
            self.fill()
        elif user_input == 'take':
            self.current_state = 'taking out the money'
            self.take()

    def show_amount(self):
        print()
        print("The coffee machine has:")
        print(str(self.water_amount) + " of water")
        print(str(self.milk_amount) + " of milk")
        print(str(self.beans_amount) + " of coffee beans")
        print(str(self.cups_amount) + " of disposable cups")
        print("$" + str(self.money_amount) + " of money")
        print()

    def buy(self, action):
        if action == '1':
            water = self.water_amount - 250
            beans = self.beans_amount - 16
            money = self.money_amount + 4
            cups = self.cups_amount - 1

            self.check_and_change_resources(water, self.milk_amount, beans, cups, money)
        elif action == '2':
            water = self.water_amount - 350
            milk = self.milk_amount - 75
            beans = self.beans_amount - 20
            money = self.money_amount + 7
            cups = self.cups_amount - 1

            self.check_and_change_resources(water, milk, beans, cups, money)
        elif action == '3':
            water = self.water_amount - 200
            milk = self.milk_amount - 100
            beans = self.beans_amount - 12
            cups = self.cups_amount - 1
            money = self.money_amount + 6

            self.check_and_change_resources(water, milk, beans, cups, money)

    def fill(self):
        print("Write how many ml of water do you want to add:")
        water = int(input())

        print("Write how many ml of milk do you want to add:")
        milk = int(input())

        print("Write how many grams of coffee beans do you want to add:")
        coffee_beans = int(input())

        print("Write how many disposable cups of coffee do you want to add:")
        cups_of_coffee = int(input())

        self.water_amount += water
        self.milk_amount += milk
        self.beans_amount += coffee_beans
        self.cups_amount += cups_of_coffee

        print()

    def take(self):
        print("I gave you $", self.money_amount)
        print()

        self.money_amount = 0

    def check_and_change_resources(self, water, milk, beans, cups, money):
        if water <= 0:
            print("Sorry, not enough water!")
        elif milk <= 0:
            print("Sorry, not enough milk!")
        elif beans <= 0:
            print("Sorry, not enough coffee beans!")
        elif cups <= 0:
            print("Sorry, not enough disposable cups!")
        else:
            print("I have enough resources, making you a coffee!")
            print()

            self.change_resources(water, milk, beans, cups, money)

        print()

    def change_resources(self, water, milk, beans, cups, money):
        self.water_amount = water
        self.milk_amount = milk
        self.beans_amount = beans
        self.cups_amount = cups
        self.money_amount = money

    def run_coffee_machine(self):
        while self.current_state != 'exiting':
            print("Write action (buy, fill, take, remaining, exit):")
            user_action = input()
            self.handle_input(user_action)

            if self.current_state == 'buying a coffee':
                print()
                print("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:")
                user_action = input()
                self.handle_input(user_action)
            elif self.current_state == 'going back to the main menu':
                continue


coffee_machine = CoffeeMachine()
coffee_machine.run_coffee_machine()
